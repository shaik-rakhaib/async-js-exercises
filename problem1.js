/* Create a directory of 3 random JSON files programatically

                                Part 1(using fs modules and callbacks)*/

// import { mkdir, writeFile, unlink } from 'fs';
// import { join } from 'path';

// // Function to create a directory
// function createDirectory1(directoryPath,n, callback) {
//     mkdir(directoryPath, { recursive: true }, (err) => {
//         if (err) {
//             callback(err);
//         } else {
//             console.log(`Directory "${directoryPath}" created.`);

//             for (let i = 0; i < n; i++) {
            //      const fileName = 'file'+(i+1).toString()+'.json';
            //      const filePath = join(directoryPath, fileName);
            //      const fileContent = 'some random content';

//                 createJsonFile1(filePath, fileContent, (fileErr) => {
//                     if (fileErr) {
//                         console.error(`Error ${fileName}: ${fileErr.message}`);
//                     } else {
//                         console.log("created", fileName);
//                         deleteFile1(filePath, (deleteErr) => {
//                             if (deleteErr) {
//                                 console.error(`Error ${fileName}: ${deleteErr.message}`);
//                             } else {
//                                 console.log("deleted", fileName);
//                             }
//                         });
//                     }
//                 });
//             }

//             callback(null);
//         }
//     });
// }

// function createJsonFile1(filePath, content, callback) {
//     const jsonContent = JSON.stringify(content, null, 2);

//     writeFile(filePath, jsonContent, (err) => {
//         if (err) {
//             callback(err);
//         } else {
//             callback(null);
//         }
//     });
// }

// function deleteFile1(filePath, callback) {
//     unlink(filePath, (err) => {
//         if (err) {
//             callback(err);
//         } else {
//             callback(null);
//         }
//     });
// }

// const directoryPath = './newlyCreatedDir';

// createDirectory1(directoryPath,3, (err) => {
//     if (err) {
//         console.error(`Error directory: ${err.message}`);
//     }
// });



/*              Part 2 (using callbacks and promises)                   */

// const fs = require('fs');
// const { join } = require('path');
// const { mkdir, writeFile, unlink } = fs.promises;

// // Function to create a directory (with callbacks and promises)
// function createDirectory2(directoryPath, callback) {
//     mkdir(directoryPath, { recursive: true })
//         .then(() => {
//             console.log(`Directory "${directoryPath}" created.`);

//             const jsonFiles = ['file1.json', 'file2.json', 'file3.json'];
//             const promises = jsonFiles.map((fileName) => {
//                 const filePath = join(directoryPath, fileName);
//                 const fileContent = { data: `Content of ${fileName}` };

//                 return new Promise((resolve, reject) => {
//                     createJsonFile2(filePath, fileContent, (fileErr) => {
//                         if (fileErr) {
//                             console.error(`Error creating ${fileName}: ${fileErr.message}`);
//                             reject(fileErr);
//                         } else {
//                             console.log("created", fileName);
//                             deleteFile2(filePath, (deleteErr) => {
//                                 if (deleteErr) {
//                                     console.error(`Error deleting ${fileName}: ${deleteErr.message}`);
//                                     reject(deleteErr);
//                                 } else {
//                                     console.log("deleted", fileName);
//                                     resolve();
//                                 }
//                             });
//                         }
//                     });
//                 });
//             });

//             return Promise.all(promises);
//         })
//         .then(() => callback(null))
//         .catch((err) => {
//             console.error(`Error: ${err.message}`);
//             callback(err);
//         });
// }

// function createJsonFile2(filePath, content, callback) {
//     const jsonContent = JSON.stringify(content, null, 2);

//     writeFile(filePath, jsonContent)
//         .then(() => callback(null))
//         .catch((err) => {
//             console.error(`Error creating file ${filePath}: ${err.message}`);
//             callback(err);
//         });
// }

// function deleteFile2(filePath, callback) {
//     unlink(filePath)
//         .then(() => callback(null))
//         .catch((err) => {
//             console.error(`Error deleting file ${filePath}: ${err.message}`);
//             callback(err);
//         });
// }

// const directoryPath = './newlyCreatedDir';

// // Usage with callbacks and promises
// createDirectory2(directoryPath, (err) => {
//     if (err) {
//         console.error(`Error creating directory: ${err.message}`);
//     }
// });

/*                              Part 3 (using async,await)                                          */
import { promises as fs } from 'fs';
import { join } from 'path';

// Function to create a directory (with async/await)
async function createDirectory3(directoryPath,n) {
    try {
        await fs.mkdir(directoryPath, { recursive: true });
        console.log(`Directory "${directoryPath}" created.`);

        for (let i = 0; i < n; i++) {
            const fileName = 'file'+(i+1).toString()+'.json';
            const filePath = join(directoryPath, fileName);
            const fileContent = 'some random content';

            try {
                await createAndDeleteFile3(filePath, fileContent);
            } catch (err) {
                console.error(`Error: ${err.message}`);
            }
        }
    } catch (err) {
        console.error(`Error: ${err.message}`);
    }
}

async function createAndDeleteFile3(filePath, content) {
    await createJsonFile3(filePath, content);
    console.log("created", filePath);

    await deleteFile3(filePath);
    console.log("deleted", filePath);
}

async function createJsonFile3(filePath, content) {
    const jsonContent = JSON.stringify(content, null, 2);
    try {
        await fs.writeFile(filePath, jsonContent);
    } catch (err) {
        throw new Error(`Error creating file ${filePath}: ${err.message}`);
    }
}

async function deleteFile3(filePath) {
    try {
        await fs.unlink(filePath);
    } catch (err) {
        throw new Error(`Error deleting file ${filePath}: ${err.message}`);
    }
}

// const directoryPath = './dir0';

// Usage with async/await
// createDirectory3(directoryPath,3);

export {createDirectory3};
