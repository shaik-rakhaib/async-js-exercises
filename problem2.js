// Problem 2
import { promises as fs } from "fs";

//function to read files
async function readFiles(filePath) {
    try {
        const data = await fs.readFile(filePath, "utf-8");
        console.log(`read ${filePath}`);
        return data;
    } catch (error) {
        console.log(error);
    }
}

//function to write to files
async function writeFiles(filePath, content,appendOrWrite) {
    try {
        if(appendOrWrite=="write"){
            await fs.writeFile(filePath, content);
        }else{
            await fs.appendFile(filePath, content);
        }
        console.log(`wrote to${filePath}`);
    } catch (error) {
        console.error(error);
    }
}

//function to convert the content to sentences and write them to diff files
async function toSentences(sentences){
    sentences.forEach(async(content,index)=>{
        await writeFiles(`./file${index+1}.txt`,content,)
        await writeFiles('./filenames.txt',`file${index+1}.txt\n`,'append')
    })
}

//function to sort the content inside the newly created files
async function sortSentences(n){
    console.log(n);
    for(let i=1;i<=n;i++){
        let data=await readFiles(`./file${i}.txt`)
        data=data.split(' ');
        data.sort();
        data=data.join(" ")
        data=data+'\n';
        await writeFiles(`./sortedFile.txt`,data,'append')
    }
}

//function to delete the files mentioned in filenames.txt
async function deletAll(fileNamesPath){
    let data=await readFiles(fileNamesPath)
    data=data.split("\n");
    data.forEach(async(element)=>{
        if(element.length){
            fs.unlink(element)
        }
    })
}

//main function to chain all the other functions
async function main() {

    //reading lisum.txt and converting them to uppercase
    const lorem_data = await readFiles("./lipsum.txt");
    const lorem_in_upper = lorem_data.toUpperCase();


    //storing the uppercase text in a new file.
    await writeFiles("./uppercase.txt", lorem_in_upper,'write');
    await writeFiles("./filenames.txt", "uppercase.txt\n",'write');

    //converting the text into array of sentences and writing each sentence to new files.
    const to_lower = (await readFiles("./uppercase.txt")).toLowerCase();
    const to_lower_sentences = to_lower.split(".");
    await toSentences(to_lower_sentences)

    //sorting the content inside the newly created files.
    await sortSentences(to_lower_sentences.length)
    await writeFiles("./filenames.txt",'sortedFile.txt\n','append')

    //deleting the files which are mentioned in the filenames.txt file.
    await deletAll('./filenames.txt')
    
}


//calling the main function
// main();

export {main};
